package com.greatLearning.main;

import com.greatLearning.assignment.*;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import com.greatLearning.assignment.Bills;
import com.greatLearning.assignment.Items;

public class Main {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Date time = new Date();
		List<Bills> bills = new ArrayList<Bills>();
		boolean finalOrder;

		List<Items> items = new ArrayList<Items>();

		Items item1 = new Items(1, "Rajma Chawal", 2, 150.0);
		Items item2 = new Items(2, "Momos", 2, 190.0);
		Items item3 = new Items(3, "Red Thai Curry", 2, 180.0);
		Items item4 = new Items(4, "Chaap", 2, 190.0);
		Items item5 = new Items(5, "Chilli Potato", 1, 250.0);

		items.add(item1);
		items.add(item2);
		items.add(item3);
		items.add(item4);
		items.add(item5);

		System.out.println(
				"Welcome to Surabhi Restaurant\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

		while (true) {
			System.out.println("Please Enter the Credentials");

			System.out.println("Email = ");
			String email = sc.next();

			System.out.println("Password = ");
			String password = sc.next();

			String name = Character.toUpperCase(password.charAt(0)) + password.substring(1);

			System.out.println("Please Enter A if you are Admin and U if you are User ,L to logout");
			String aorU = sc.next();

			Bills bill = new Bills();
			List<Items> selectedItems = new ArrayList<Items>();

			double totalCost = 0;

			Date date = new Date();

			ZonedDateTime time1 = ZonedDateTime.now();
			DateTimeFormatter f = DateTimeFormatter.ofPattern("E MMM dd HH:mm:ss zzz yyyy");
			String currentTime = time1.format(f);

			if (aorU.equals("U") || aorU.equals("u")) {

				System.out.println("Welcome Mr. " + name);
				do {
					System.out.println("Today's Menu :- ");
					items.stream().forEach(i -> System.out.println(i));
					System.out.println("Enter the Menu Item Code");
					int code = sc.nextInt();

					if (code == 1) {
						selectedItems.add(item1);
						totalCost += item1.getItemPrice();
					}

					else if (code == 2) {
						selectedItems.add(item2);
						totalCost += item2.getItemPrice();
					} else if (code == 3) {
						selectedItems.add(item3);
						totalCost += item3.getItemPrice();
					} else if (code == 4) {
						selectedItems.add(item4);
						totalCost += item4.getItemPrice();
					} else {
						selectedItems.add(item5);
						totalCost += item5.getItemPrice();
					}

					System.out.println("Press 0 to show bill\nPress 1 to order more");
					int opt = sc.nextInt();
					if (opt == 0)
						finalOrder = false;
					else
						finalOrder = true;

				} while (finalOrder);

				System.out.println("Thanks Mr " + name + " for dining in with Surabi ");
				System.out.println("Items you have Selected");
				selectedItems.stream().forEach(e -> System.out.println(e));
				System.out.println("Your Total bill will be " + totalCost);

				bill.setName(name);
				bill.setCost(totalCost);
				bill.setItems(selectedItems);
				bill.setTime(date);
				bills.add(bill);

			} else if (aorU.equals("A") || aorU.equals("a")) {
				System.out.println("Welcome Admin");
				System.out.println(
						"Press 1 to see all the bills for today\nPress 2 to see all the bills for this month\nPress 3 to see all the bills");
				int option = sc.nextInt();
				switch (option) {
				case 1:
					if (!bills.isEmpty()) {
						for (Bills b : bills) {
							if (b.getTime().getDate() == time.getDate()) {
								System.out.println("\nUsername :- " + b.getName());
								System.out.println("Items :- " + b.getItems());
								System.out.println("Total :- " + b.getCost());
								System.out.println("Date " + b.getTime() + "\n");
							}
						}
					} else
						System.out.println("No Bills today.!");
					break;

				case 2:
					if (!bills.isEmpty()) {
						for (Bills b : bills) {
							if (b.getTime().getMonth() == time.getMonth()) {
								System.out.println("\nUsername :- " + b.getName());
								System.out.println("Items :- " + b.getItems());
								System.out.println("Total :- " + b.getCost());
								System.out.println("Date " + b.getTime() + "\n");
							}
						}
					} else
						System.out.println("No Bills for this month.!");
					break;

				case 3:
					if (!bills.isEmpty()) {
						for (Bills b : bills) {
							System.out.println("\nUsername :- " + b.getName());
							System.out.println("Items :- " + b.getItems());
							System.out.println("Total :- " + b.getCost());
							System.out.println("Date " + b.getTime() + "\n");
						}
					} else
						System.out.println("No Bills.!");

					break;

				default:
					System.out.println("Invalid Option");
					System.exit(1);
				}
			} else if (aorU.equals("L") || aorU.equals("l")) {
				System.exit(1);

			} else {
				System.out.println("Invalid Entry");
			}

		}

	}

}
