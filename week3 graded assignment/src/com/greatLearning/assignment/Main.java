package com.greatLearning.assignment;

import java.util.*;

public class Main {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		MagicOfBooks mb = new MagicOfBooks();
		while (true) {
			System.out.println("Choose one option from below\n" + "1.To add books\n" + "2.To delete entries from book\n"
					+ "3.To update a book\n" + "4.To display all books\n" + "5.Total count of Books\n"
					+ "6.Search Autobiography books\n" + "7.To display by features");
			System.out.println("Enter your Choice: ");
			int choice = sc.nextInt();
			switch (choice) {
			case 1:// adding a book
				System.out.println("enter no of books u want to add: ");
				int n = sc.nextInt();
				for (int i = 1; i <= n; i++) {
					mb.addBook();
				}
				break;
			case 2:// Deleting book
				try {
					mb.deletebook();
				} catch (CustomException e) {
					System.out.println(e.getMessage());

				}
				break;
			case 3:// update a book
				try {
					mb.updatebook();
				} catch (CustomException e) {
					System.out.println(e.getMessage());
				}
				break;
			case 4:// displaying books
				try {
					mb.displayBooksInfo();
				} catch (CustomException e) {
					System.out.println(e.getMessage());
				}
				break;
			case 5:// counting books
				try {
					mb.count();
				} catch (CustomException e) {
					System.out.println(e.getMessage());
				}
				break;
			case 6: // autobiography search
				try {
					mb.autobiography();
				} catch (CustomException e) {
					System.out.println(e.getMessage());
				}
				break;
			case 7:// search by feature
				System.out.println(
						"Enter your choice: \n 1.price low to high " + "\n 2. price high to low\n 3. best selling");
				int ch = sc.nextInt();
				switch (ch) {
				case 1:
					try {
						mb.displayByFeature(1);
					} catch (CustomException e) {
						System.out.println(e.getMessage());
					}
					break;
				case 2:
					try {
						mb.displayByFeature(2);
					} catch (CustomException e) {
						System.out.println(e.getMessage());
					}
					break;
				case 3:
					try {
						mb.displayByFeature(3);
					} catch (CustomException e) {
						System.out.println(e.getMessage());
					}
					break;
				}
			default:
				System.out.println("you have entered wrong choice ");
			}
		}
	}

}
