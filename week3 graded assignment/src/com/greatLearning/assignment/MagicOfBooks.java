package com.greatLearning.assignment;

import java.util.*;

public class MagicOfBooks<T> {
	Scanner sc = new Scanner(System.in);
	HashMap<Integer, BookDetails> bookmap = new HashMap<>();
	TreeMap<Double, BookDetails> treemap = new TreeMap<>();
	ArrayList<BookDetails> booklist = new ArrayList<>();

	// adding books
	public void addBook() {
		BookDetails b = new BookDetails();
		System.out.println("Enter book id: ");
		b.setId(sc.nextInt());

		System.out.println("Enter book name: ");
		b.setName(sc.next());

		System.out.println("Enter book price: ");
		b.setPrice(sc.nextDouble());

		System.out.println("Enter book genre(Autobiography-Autobiography): ");
		b.setGenre(sc.next());

		System.out.println("Enter number of copies sold: ");
		b.setNoOfCopiesSold(sc.nextInt());

		System.out.println("Enter book status(B-bestselling)" + ": ");
		b.setBookStatus(sc.next());

		bookmap.put(b.getId(), b);
		System.out.println("Book added Successfully");

		treemap.put(b.getPrice(), b);
		booklist.add(b);

	}

	// deleting books
	public void deletebook() throws CustomException {
		if (bookmap.isEmpty()) {
			throw new CustomException("no books are available to delete!!");
		} else {
			System.out.println("enter bookid you want to delete: ");
			int id = sc.nextInt();
			bookmap.remove(id);
			System.out.println("Successfully deleted");
		}
	}

	// updating books
	public void updatebook() throws CustomException {
		if (bookmap.isEmpty()) {
			throw new CustomException("No books are available to update!!");
		} else {
			BookDetails b = new BookDetails();
			System.out.println("enter book id: ");
			b.setId(sc.nextInt());
			System.out.println("Enter book name: ");
			b.setName(sc.next());
			System.out.println("Enter book price: ");
			b.setPrice(sc.nextDouble());
			System.out.println("Enter book genre: ");
			b.setGenre(sc.next());
			System.out.println("Enter number of books sold: ");
			b.setNoOfCopiesSold(sc.nextInt());
			System.out.println("Enter book Status: ");
			b.setBookStatus(sc.next());
			System.out.println("Book details updated Successfully: ");
			bookmap.replace(b.getId(), b);
			System.out.println("book updated succesfully");
		}
	}

	// displaying books
	public void displayBooksInfo() throws CustomException {
		if (bookmap.size() > 0) {
			Set<Integer> keySet = bookmap.keySet();
			for (Integer key : keySet) {
				System.out.println(key + " " + bookmap.get(key));
			}
		} else {
			throw new CustomException("BooksMap is Empty!!");
		}
	}

	// counting
	public void count() throws CustomException {
		if (bookmap.isEmpty()) {
			throw new CustomException("Books store is Empty!!");
		} else
			System.out.println("Number of books present in the store : " + bookmap.size());
	}

	// autobiography search
	public void autobiography() throws CustomException {
		String bestSelling = "AutoBiography";
		if (booklist.isEmpty()) {
			throw new CustomException("Books store is Empty!!");
		} else {
			ArrayList<BookDetails> genreBookList = new ArrayList<BookDetails>();
			Iterator<BookDetails> itr = (booklist.iterator());
			while (itr.hasNext()) {
				BookDetails b1 = (BookDetails) itr.next();
				if (b1.genre.equals(bestSelling)) {
					genreBookList.add(b1);
					System.out.println(genreBookList);
				}
			}
		}
	}

	// displaying by feature
	public void displayByFeature(int flag) throws CustomException {
		if (flag == 1) {
			if (bookmap.size() > 0) {
				Set<Double> keySet = treemap.keySet();
				for (Double key : keySet) {
					System.out.println(key + " " + treemap.get(key));
				}

			} else {
				throw new CustomException("Book store is empty!!");
			}
		}
		if (flag == 2) {
			Map<Double, Object> treeMapReverseOrder = new TreeMap<>(treemap);
			// putting value in navigable map
			NavigableMap<Double, Object> nmap = ((TreeMap<Double, Object>) treeMapReverseOrder).descendingMap();
			System.out.println("Book Deatils: ");
			if (nmap.size() > 0) {
				Set<Double> keySet = nmap.keySet();
				for (Double key : keySet) {
					System.out.println(key + " " + nmap.get(key));
				}
			} else {
				throw new CustomException("Book store is Empty!!");
			}
		}
		if (flag == 3) {
			String bestSelling = "B";
			ArrayList<BookDetails> genreBookList = new ArrayList<BookDetails>();
			Iterator<BookDetails> itr = booklist.iterator();
			while (itr.hasNext()) {
				BookDetails b = (BookDetails) itr.next();
				if (b.bookStatus.equals(bestSelling)) {
					genreBookList.add(b);
				}
			}
			if (genreBookList.isEmpty()) {
				throw new CustomException("no best selling books are available!!");
			} else
				System.out.println("best selling books: " + genreBookList);

		}

	}
}
