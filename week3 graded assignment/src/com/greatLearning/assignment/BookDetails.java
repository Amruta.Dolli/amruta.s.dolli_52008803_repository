package com.greatLearning.assignment;

import java.util.*;

public class BookDetails {
	int id;
	String name;
	Double price;
	String genre;
	int noOfCopiesSold;
	String bookStatus;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public int getNoOfCopiesSold() {
		return noOfCopiesSold;
	}

	public void setNoOfCopiesSold(int noOfCopiesSold) {
		this.noOfCopiesSold = noOfCopiesSold;
	}

	public String getBookStatus() {
		return bookStatus;
	}

	public void setBookStatus(String bookStatus) {
		this.bookStatus = bookStatus;
	}

	@Override
	public String toString() {
		return "BookDetails [id=" + id + ", name=" + name + ", price=" + price + ", genre=" + genre
				+ ", noOfCopiesSold=" + noOfCopiesSold + ", bookStatus=" + bookStatus + "]";
	}

}
