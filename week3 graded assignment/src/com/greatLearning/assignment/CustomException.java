package com.greatLearning.assignment;

public class CustomException extends Exception {
	public CustomException(String str) {
		super(str);
	}
}
