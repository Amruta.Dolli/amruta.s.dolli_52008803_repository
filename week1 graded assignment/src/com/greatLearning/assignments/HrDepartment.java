package com.greatLearning.assignments;

public class HrDepartment extends SuperDepartment {
	// declare method departmentName of return type string
	public String departmentName() {
		return "Hr Department";
	}

	// declare method getTodaysWork of return type string
	public String getTodaysWork() {
		return "Fill todays worksheet and mark your attendence";
	}

	// declare method doActivity of return type string
	public String doActivity() {
		return "Team Launch";
	}

	// declare method getWorkDeadline of return type string
	public String getWorkDeadline() {
		return "Complete by End of Day";
	}

}
