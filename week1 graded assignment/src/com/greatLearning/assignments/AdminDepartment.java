package com.greatLearning.assignments;

public class AdminDepartment extends SuperDepartment{
	//declare method departmentName of return type string
	public String departmentName() {
		return "Admin Department";
	}
	//declare method getTodaysWork of return type string
	public String getTodaysWork() {
		return "Complete your documents";
	}

	//declare method getWorkDeadline of return type string
	public String getWorkDeadline() {
		return "Complete by End of Day";
	}

}
