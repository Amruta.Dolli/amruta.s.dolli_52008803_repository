package com.greatLearning.assignments;

public class TechDepartment extends SuperDepartment {

	// declare method departmentName of return type string
	public String departmentName() {
		return "Tech Department";
	}

	// declare method getTodaysWork of return type string
	public String getTodaysWork() {
		return "Complete coding of module 1";
	}

	// declare method getTechStackInformation of return type string
	public String getTechStackInformation() {
		return "Core Java";
	}

	// declare method getWorkDeadline of return type string
	public String getWorkDeadline() {
		return "Complete by End of day";
	}

}
