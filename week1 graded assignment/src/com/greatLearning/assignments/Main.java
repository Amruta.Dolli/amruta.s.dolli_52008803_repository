package com.greatLearning.assignments;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		SuperDepartment sp = new SuperDepartment();
		System.out.println("Super department Deatils");
		System.out.println(sp.departmentName());
		System.out.println(sp.getTodaysWork());
		System.out.println(sp.getWorkDeadline());
		System.out.println(sp.isTodayAHoliday());
		System.out.println(" ");

		while (true) {
			Scanner sc = new Scanner(System.in);
			System.out.println("enter 1 for admin department\n" + "enter 2 for hr department\n"
					+ "enter 3 for tech department\n" + "enter 4 for exit");
			System.out.print("enter option:");
			int userInput = sc.nextInt();
			System.out.println(" ");

			switch (userInput) {

			case 1:
				AdminDepartment ad = new AdminDepartment();
				System.out.println(ad.departmentName());
				System.out.println(ad.getTodaysWork());
				System.out.println(ad.getWorkDeadline());
				System.out.println("Today is holiday or not using superclass: " + sp.isTodayAHoliday());
				System.out.println(" ");
				break;
			case 2:
				HrDepartment hd = new HrDepartment();
				System.out.println(hd.departmentName());
				System.out.println(hd.getTodaysWork());
				System.out.println(hd.doActivity());
				System.out.println(hd.getWorkDeadline());
				System.out.println("Today is holiday or not using superclass: " + sp.isTodayAHoliday());
				System.out.println(" ");
				break;
			case 3:
				TechDepartment td = new TechDepartment();
				System.out.println(td.departmentName());
				System.out.println(td.getTodaysWork());
				System.out.println(td.getTechStackInformation());
				System.out.println(td.getWorkDeadline());
				System.out.println("Today is holiday or not using superclass: " + sp.isTodayAHoliday());
				System.out.println(" ");
				break;
			case 4:
				System.exit(1);
			default:
				System.out.println("Enter invalid option");

			}
		}
	}

}
