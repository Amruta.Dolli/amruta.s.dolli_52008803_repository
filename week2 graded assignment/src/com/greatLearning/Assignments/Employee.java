package com.greatLearning.Assignments;

import java.util.*;

public class Employee implements Comparable<Employee> {
	int id;
	String name;
	int age;
	int salary;
	String department;
	String city;

	public Employee(int id, String name, int age, int salary, String department, String city)
			throws IllegalArgumentException {
		super();

		if (id <= 0) {
			throw new IllegalArgumentException("Exception occured: id is not less than zero,enter correct id no");
		}
		this.id = id;
		if (name == null && name.isEmpty()) {
			throw new IllegalArgumentException("Exception occured:Employee name shold not be null,enter correct name");
		}

		this.name = name;

		if (age <= 0) {
			throw new IllegalArgumentException("Exception occured:age is more then zero ");
		}
		this.age = age;

		if (salary <= 0) {
			throw new IllegalArgumentException("Exception occured:Salary cannot be zero");
		}

		this.salary = salary;

		if (department == null && department.isEmpty()) {
			throw new IllegalArgumentException("Exeption occured: department cannot be null");
		}
		this.department = department;

		if (city == null && city.isEmpty()) {
			throw new IllegalArgumentException("exception occured:City cannot be null");
		}

		this.city = city;

	}

	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {

		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public int compareTo(Employee o) {
		return this.getName().compareTo(o.getName());
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", age=" + age + ", salary=" + salary + ", department="
				+ department + ", city=" + city + "]";
	}

}