package com.greatLearning.Assignments;

import java.util.*;

public class DataStructureA {
	public void sortingNames(ArrayList<Employee> employees) {

		Collections.sort(employees);
		for (Employee e : employees) {
			System.out.print(e.getName() + " ");
		}
		System.out.println(" ");
	}
}
