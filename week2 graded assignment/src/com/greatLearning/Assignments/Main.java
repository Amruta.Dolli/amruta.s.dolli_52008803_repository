package com.greatLearning.Assignments;

import java.util.*;

public class Main {
	public static void main(String args[]) {
		ArrayList<Employee> employees = new ArrayList<>();
		Employee e1 = new Employee(1, "Aman", 20, 1100000, "IT","Delhi");
		Employee e2 = new Employee(2, "Bobby", 22, 500000, "HR", "Bombay");
		Employee e3 = new Employee(3, "Zoe", 20, 750000, "ADMIN", "Delhi");
		Employee e4 = new Employee(4, "Smitha", 21, 1000000, "IT", "chennai");
		Employee e5 = new Employee(5, "Smitha", 24, 1200000, "HR", "bangalore");

		employees.add(e1);
		employees.add(e2);
		employees.add(e3);
		employees.add(e4);
		employees.add(e5);

		System.out.println("employee details are:");
		for (Employee emp : employees)
			System.out.println(emp);
		System.out.println("\n");

		System.out.println("Montly salary");
		DataStructureB salary = new DataStructureB();
		salary.monthlySalary(employees);
		System.out.println(" ");
		
		System.out.println("count of employees from each city...");
		salary.cityNameCount(employees);
		System.out.println(" ");

		System.out.println("Sorting according to names");
		DataStructureA name = new DataStructureA();
		name.sortingNames(employees);
		System.out.println(" ");

	}
}
