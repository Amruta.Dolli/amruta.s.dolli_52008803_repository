package com.greatLearning.Assignments;

import java.util.*;

public class DataStructureB {
	public void cityNameCount(ArrayList<Employee> employees) {
		Set<String> set = new HashSet<String>();
		ArrayList<String> list = new ArrayList<String>();

		for (Employee emp : employees) {
			list.add(emp.getCity());
			set.add(emp.getCity());
		}
		for (String emp : set) {
			System.out.print(emp + "=" + Collections.frequency(list, emp) + " ");

		}
		System.out.println(" ");

	}

	public void monthlySalary(ArrayList<Employee> employees) {

		for (Employee emp : employees) {
			System.out.print(emp.getId() + "=" + emp.getSalary() / 12 + " ");
		}
		System.out.println(" ");
	}

}
